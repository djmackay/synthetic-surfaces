#include "Plane.hpp"
#include <iostream>

double Plane::distanceToPoint(glm::vec3 vertex) {
    double d = -(point.x * normal.x + point.y * normal.y + point.z * normal.z);
    double dotProd = glm::dot(normal, vertex);
    double distance = dotProd + d;

    return distance;
}