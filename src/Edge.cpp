#include "Edge.hpp"


glm::vec3 Edge::direction() {
    return end - start;
}


double Edge::length() {
    auto dir = direction();
    return sqrt(pow(dir.x, 2)+pow(dir.y, 2)+pow(dir.z, 2));
}