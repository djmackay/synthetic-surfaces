#include <iostream>
#include "PointSampler.hpp"

namespace PointSampler {
    using std::vector;
    vector<glm::vec3> samplePoints(Edge edge, double distance)
    {
        vector<glm::vec3> sampledPoints;
        auto lineLength = edge.length();
        auto lineDirection = glm::normalize(edge.direction());

        if (distance > lineLength) {
            glm::vec3 newPoint = edge.start + lineDirection * static_cast<float>(lineLength / 2.0);
            sampledPoints.emplace_back(newPoint);
            return sampledPoints;
        }
        auto current = 0.5f * static_cast<float>(distance);
        while (current < lineLength) {
            glm::vec3 newPoint = edge.start+lineDirection * current;
            sampledPoints.emplace_back(newPoint);
            current += distance;
        }

        return sampledPoints;
    }

    SampledPoints samplePointsRemainder(Edge edge, double distance, double remainder)
    {
        SampledPoints sampledPoints;
        sampledPoints.remainder = remainder;

        auto lineLength = edge.length();
        auto lineDirection = glm::normalize(edge.direction());

        if (lineLength < remainder) {
            sampledPoints.remainder -= lineLength;
            std::cout << sampledPoints.remainder << "\n";
            return sampledPoints;
        }

        auto current = 0.5f * static_cast<float>(distance);

        if (sampledPoints.remainder < distance) {
            current = 0.5f * static_cast<float>(sampledPoints.remainder);
        }

        while (current < lineLength) {
            glm::vec3 newPoint = edge.start+lineDirection * current;
            sampledPoints.points.emplace_back(newPoint);
            current += distance;
            sampledPoints.remainder -= distance;
        }
        sampledPoints.remainder -= lineLength;

        return sampledPoints;
    }
}  // namespace PointSampler.