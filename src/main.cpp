#include <iostream>
#include <vector>

#include <SurfaceImporter.hpp>
#include "ContourExporter.hpp"
#include "Intersection.hpp"
#include "PointSampler.hpp"
#include "PointSet.hpp"
#include "Synthetic.cpp"
#include "Timer.hpp"

void planeLineIntersectionTest() {
    glm::vec3 planeTestNormal{0.0, 1.0, 0.0};
    glm::vec3 planeTestPoint{0.0, 5.0, 0.0};
    Plane testPlane{planeTestPoint, planeTestNormal};

    glm::vec3 start{2.0, 0.0, 0.0};
    glm::vec3 end{3.0, 10.0, 0.0};
    Edge testLine{start, end};

    if (Intersection::intersects(testPlane, testLine)) {
        auto result = Intersection::computeIntersection(testPlane, testLine);
        cout << "Expected intersection at (2.5, 5, 0)." << endl;
        cout << "Result: (" << result.x << "," << result.y << "," << result.z << ")" << endl;
    }
}

void intersectionTest() {
    glm::vec3 planeTestNormal{0.0, 1.0, 0.0};
    glm::vec3 planeTestPoint{0.0, 5.0, 0.0};
    Plane testPlane{planeTestPoint, planeTestNormal};

    glm::vec3 v1{-2.0, 3.0, 0.0};
    glm::vec3 v2{0.0, 7.0, 0.0};
    glm::vec3 v3{2.0, 3.0, 0.0};
    Triangle t1{v1, v2, v3};

    if (Intersection::intersects(testPlane, t1)) {
        cout << "intersects!" << endl;
        auto result = Intersection::computeIntersection(testPlane, t1);
        cout << "Expected intersection from (-1, 5, 0) to (1, 5, 0)" << endl;
        cout << "Result: (" << result.start.x << "," << result.start.y << "," << result.start.z << ")";
        cout << " -> (" << result.end.x << "," << result.end.y << "," << result.end.z << ")" << endl;
    }
}


void pointSamplerTest() {
    glm::vec3 start{0.0, 3.0, -46.5};
    glm::vec3 end{5.0, 7.0, 21.0};
    Edge edge{start, end};

    vector<glm::vec3> points = PointSampler::samplePoints(edge, 1.0);

    for (auto point: points) {
        cout << "Sampled Point: (" << point.x << "," << point.y << "," << point.z << ")" << endl;
    }
}


void pointSamplerMidPointTest() {
    glm::vec3 start{0.0, 0.0, 0.0};
    glm::vec3 end{10.0, 0.0, 0.0};
    Edge edge{start, end};

    vector<glm::vec3> points = PointSampler::samplePoints(edge, 20.0);

    for (auto point: points) {
        cout << "Sampled Point: (" << point.x << "," << point.y << "," << point.z << ")" << endl;
    }
}


void pointDistanceTest() {
    glm::vec3 point{3.0, 4.0, 5.0};
    Plane testPlane{glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0)};

    auto distance = testPlane.distanceToPoint(point);
    cout << "Distance to plane: " << distance << endl;
}


void centroidTest() {
    glm::vec3 p1{0.0, 10.0, 0.0};
    glm::vec3 p2{0.0, -10.0, 0.0};
    glm::vec3 p3{20.0, 0.0, 0.0};
    glm::vec3 p4{-10.0, 0.0, 0.0};

    PointSet pointSet;
    pointSet.addPoint(p1);
    pointSet.addPoint(p2);
    pointSet.addPoint(p3);
    pointSet.addPoint(p4);

    auto centroid = pointSet.computeCentroid();

    cout << "Expected centroid: (2.5, 0.0, 0.0)." << endl;
    cout << "Found centroid: (" << centroid.x << ", " << centroid.y << ", " << centroid.z << ")" << endl;
}

void orderedPointSetTest() {
    // Creates an unordered point set and attempts to retrieve an ordered one.
    glm::vec3 p1{2.0, 0.0, 1.0};
    glm::vec3 p2{-1.0, 0.0, 2.0};
    glm::vec3 p3{-2.0, 0.0, -1.0};
    glm::vec3 p4{1.0, 0.0, -2.0};

    PointSet pointSet;
    pointSet.addPoint(p2);
    pointSet.addPoint(p3);
    pointSet.addPoint(p1);
    pointSet.addPoint(p4);

    auto centroid = pointSet.computeCentroid();

    vector<glm::vec3> points = pointSet.toOrderedPointSet(centroid);

    cout << "Ordered points: " << endl;
    for (auto point: points) {
        cout << "\t(" << point.x << ", " << point.y << ", " << point.z << ")" << endl;
    }
}

void computeAngleTest() {
    glm::vec3 p0{0.0, 0.0, 0.0};
    glm::vec3 p1{2.0, 0.0, 2.0};
    glm::vec3 p2{-2.0, 0.0, 2.0};
    glm::vec3 p3{-2.0, 0.0, -2.0};
    glm::vec3 p4{2.0, 0.0, -2.0};

    vector<glm::vec3> points;
    points.emplace_back(p1);
    points.emplace_back(p2);
    points.emplace_back(p3);
    points.emplace_back(p4);

    for (auto point: points) {
        auto angle = computeAngle(p0, point);
        cout << "Angle from x axis to point: (" << point.x << ", " << point.y << ", " << point.z << ")" << endl;
        cout << "\t" << angle << endl;
    }
}


void samplerTest() {
    const SamplerConfig samplerConfig = {0.01, 0.32};

    glm::vec3 p1{0.0, 1.0, 0.0};
    glm::vec3 p2{3.0, 1.0, 0.0};
    glm::vec3 p3{3.0, 1.0, 3.0};
    glm::vec3 p4{0.0, 1.0, 3.0};
    glm::vec3 p5{0.0, 4.0, 0.0};
    glm::vec3 p6{3.0, 4.0, 0.0};
    glm::vec3 p7{3.0, 4.0, 3.0};
    glm::vec3 p8{0.0, 4.0, 3.0};

    Triangle t1{p2, p1, p5};
    Triangle t2{p2, p5, p6};
    Triangle t3{p3, p2, p6};
    Triangle t4{p3, p6, p7};
    Triangle t5{p4, p3, p7};
    Triangle t6{p3, p7, p8};
    Triangle t7{p1, p4, p8};
    Triangle t8{p1, p8, p5};

    vector<Triangle> triangles;
    triangles.emplace_back(t1);
    triangles.emplace_back(t2);
    triangles.emplace_back(t3);
    triangles.emplace_back(t4);
    triangles.emplace_back(t5);
    triangles.emplace_back(t6);
    triangles.emplace_back(t7);
    triangles.emplace_back(t8);

    auto contours = createContours(triangles, samplerConfig);
    toXYZ(contours, "text.xyz");
}

void contourGenerationTest() {
    const SamplerConfig samplerConfig = {0.01, 0.32};

    glm::vec3 p1{0.0, 1.0, 0.0};
    glm::vec3 p2{3.0, 1.0, 0.0};
    glm::vec3 p3{3.0, 1.0, 3.0};
    glm::vec3 p4{0.0, 1.0, 3.0};
    glm::vec3 p5{0.0, 4.0, 0.0};
    glm::vec3 p6{3.0, 4.0, 0.0};
    glm::vec3 p7{3.0, 4.0, 3.0};
    glm::vec3 p8{0.0, 4.0, 3.0};

    Triangle t1{p2, p1, p5};
    Triangle t2{p2, p5, p6};
    Triangle t3{p3, p2, p6};
    Triangle t4{p3, p6, p7};
    Triangle t5{p4, p3, p7};
    Triangle t6{p3, p7, p8};
    Triangle t7{p1, p4, p8};
    Triangle t8{p1, p8, p5};

    vector<Triangle> triangles;
    triangles.emplace_back(t1);
    triangles.emplace_back(t2);
    triangles.emplace_back(t3);
    triangles.emplace_back(t4);
    triangles.emplace_back(t5);
    triangles.emplace_back(t6);
    triangles.emplace_back(t7);
    triangles.emplace_back(t8);

    auto contours = createContours(triangles, samplerConfig);

    toTxt(contours, "text.txt");
}


void readFromPlyTest() {
    string filename = "test.ply";
    Geometry::Surface surface = SurfaceImporter::loadSurfacePly(filename);
    std::cout << surface.toString();
}

void readAndOutputTest() {
    string filename = "test2.ply";
    Geometry::Surface surface = SurfaceImporter::loadSurfacePly(filename);
    const SamplerConfig samplerConfig = {0.1, 1.0};
    auto contours = createContours(surface.toTriangles(), samplerConfig);
    toXYZ(contours, "new.xyz");
    toTxt(contours, "new.txt");
    toSeperateContours(contours);
}

void getContourInformation(std::vector<std::vector<std::vector<glm::vec3>>> contours) {
    std::cout << "There are " << contours.size() << " contour lists (sampled heights).\n";
    for (auto contourList : contours) {
        std::cout << "\tThere are " << contourList.size() << " contours at this height\n";
        for (auto& contour : contourList) {
            std::cout << "\t\tThere are " << contour.size() << " points in this contour\n";
        }
    }
}

void readAndOutputFormattedTest(const string& filename)
{
    Geometry::Surface surface = SurfaceImporter::loadSurfacePly(filename);
    const ContourExporter::SamplerConfig samplerConfig = {0.1, 1.5};
    auto contours = ContourExporter::createContoursFormatted(surface.toTriangles(), samplerConfig);
    getContourInformation(contours);
    ContourExporter::toWhitespaceFormattedFile(contours, "synthetic-output.txt");
    ContourExporter::toXyzFormatted(contours, "synthetic-output.xyz");
}

void testFindDistinctPoints() {
    glm::vec3 p1{0.0, 1.0, 0.0};
    glm::vec3 p2{0.1, 1.0, 0.0};
    glm::vec3 p3{0.2, 1.0, 0.0};
    glm::vec3 p4{0.3, 1.0, 0.0};
    glm::vec3 p5{1.0, 4.0, 0.0};
    glm::vec3 p6{1.1, 4.0, 0.0};

    vector<glm::vec3> points;
    points.emplace_back(p1);
    points.emplace_back(p4);
    points.emplace_back(p2);
    points.emplace_back(p3);
    points.emplace_back(p5);
    points.emplace_back(p6);

    auto contours = findDistinctPointSets(points, 0.1);
    for (auto contour: contours) {
        cout << "Contour: \n";
        for (auto point: contour) {
            cout << "\tPoint(x,y,z): (" << point.x << "," << point.y << "," << point.z << ")\n";
        }
    }
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Provide mesh filename.\n";
        return 0;
    }

    std::string meshFilename = argv[1];

    Timer timer;

    timer.start();
    readAndOutputFormattedTest(meshFilename);
    timer.stop();
    timer.printElapsedTime("Read from ply and output xyz.");

    return 0;
}