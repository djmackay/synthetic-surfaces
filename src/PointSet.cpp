#include "PointSet.hpp"

using std::vector;

glm::vec3 PointSet::computeCentroid() {
    glm::vec3 total{0.0, 0.0, 0.0};

    for (auto point: points) {
        total += point;
    }

    auto len = points.size();
    glm::vec3 result = total / static_cast<float>(len);

    return result;
}


void PointSet::addPoint(glm::vec3 point) {
    points.emplace_back(point);
}


void PointSet::addPoints(std::vector<glm::vec3> inputPoints) {
    points.insert(points.end(), inputPoints.begin(), inputPoints.end());
}




vector<glm::vec3> PointSet::toOrderedPointSet(glm::vec3 centroid) {
    sort(points.begin(), points.end(),
            [centroid](const glm::vec3& a, const glm::vec3& b) -> bool
            {
                return computeAngle(centroid, a) < computeAngle(centroid, b);
            });

    return points;
}

double computeAngle(glm::vec3 centroid, glm::vec3 point) {
    glm::vec3 xAxis{1, centroid.y, 0};
    glm::vec3 directionVector = glm::normalize(point - centroid);

    double angle = glm::angle(directionVector, xAxis);

    if (directionVector.z < 0.0) {
        const double pi = 3.14159265358979323846;
        angle = 2 * pi - angle;
    }

    return angle;
}
