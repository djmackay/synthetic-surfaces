#include "ContourExporter.hpp"

namespace ContourExporter {
    using std::vector;
    using std::string;
    using PointSampler::SampledPoints;

    vector<vector<glm::vec3>> findDistinctPointSets(vector<glm::vec3> points, double threshold) {
        // Begin at a random point, create a new contour.
        vector<vector<glm::vec3>> contours;
        vector<glm::vec3> currentContour;

        // Get the first point from the vector and remove it.
        auto currentPoint = points.front();
        points.erase(points.begin());
        currentContour.emplace_back(currentPoint);

        while (!points.empty()) {
            auto closest = points.begin();
            double distToClosest = std::numeric_limits<double>::infinity();
            // Iterate through all remaining points. Find the closest point and add it to the contour.
            for (auto it = points.begin(); it != points.end(); it++) {
                if (glm::distance(currentPoint, *it) < distToClosest) {
                    closest = it;
                    distToClosest = glm::distance(currentPoint, *closest);
                }
            }

            // If we found no points to add, we must create a new contour
            if (std::isinf(distToClosest) || distToClosest > threshold) {
                contours.emplace_back(currentContour);
                // Get the first point in the vector, and erase it.
                currentPoint = points.front();
                points.erase(points.begin());
                // Reset the contour vector, then add the first point to it.
                currentContour.clear();
                currentContour.emplace_back(currentPoint);
            } else {
                currentPoint = *closest;
                currentContour.emplace_back(*closest);
                points.erase(closest);
            }
        }

        contours.emplace_back(currentContour);

        return contours;
    }

    // Create a set of contours from a set of triangles.
    vector<vector<vector<glm::vec3>>> createContoursFormatted(vector<Triangle> triangles, SamplerConfig samplerConfig)
    {
        if (samplerConfig.contourDistance<=0.0) {
            throw SamplerConfigurationError();
        }

        double startHeight;
        double maxHeight;
        std::tie(startHeight, maxHeight) = getLowestHighestPoint(triangles);

        const double initialStep = 0.0001;
        const double numSlices = 10;

        startHeight += initialStep; // Adjust initial sampling value
        const double endHeight = maxHeight- initialStep;

        const double step = (endHeight - startHeight) / (numSlices-1);

        vector<vector<vector<glm::vec3>>> contours;
        double remainder = samplerConfig.pointDistance;

        double currentHeight = startHeight;
        while (currentHeight<maxHeight) {
            // Create a plane at current height.
            vector<vector<glm::vec3>> contourList;
            Plane plane{glm::vec3{0.0, currentHeight, 0.0}, glm::vec3{0.0, 1.0, 0.0}};
            vector<glm::vec3> points;
            // Generate edges, sample points and add to (currently global) point set.
            for (auto triangle: triangles) {
                if (Intersection::intersects(plane, triangle)) {
                    auto edge = Intersection::computeIntersection(plane, triangle);
                    if (remainder < 0) {
                        std::cout << remainder << " reset\n";
                        remainder = samplerConfig.pointDistance;
                    }
                    SampledPoints sampledPoints = PointSampler::samplePointsRemainder(edge,
                            samplerConfig.pointDistance, remainder);
                    auto newPoints = sampledPoints.points;
                    remainder = sampledPoints.remainder;
                    if (!newPoints.empty()) {
                        points.insert(points.end(), newPoints.begin(), newPoints.end());
                    }
                }
            }

            if (points.empty()) {
                std::cout << "No points generated. Configuration distance may be too high.\n";
                throw SamplerConfigurationError();
            }
            double threshold = samplerConfig.pointDistance*3;
            auto distinctPointSets = findDistinctPointSets(points, threshold);

            for (auto& pointSet : distinctPointSets) {
                PointSet unordered;
                unordered.addPoints(pointSet);
                auto orderedPoints = unordered.toOrderedPointSet(unordered.computeCentroid());
                if (!orderedPoints.empty()) {
                    contourList.emplace_back(orderedPoints);
                }
            }

            contours.emplace_back(contourList);
            currentHeight += step;
        }
        return contours;
    }

    void toWhitespaceFormattedFile(vector<vector<vector<glm::vec3>>> contours, string filename) {
        std::ofstream outFile;
        outFile.open(filename);

        for (auto contourList : contours) {
            for (auto contour : contourList) {
                for (auto point : contour) {
                    outFile << std::to_string(point.x) + ",";
                    outFile << std::to_string(point.y) + ",";
                    outFile << std::to_string(point.z);
                    outFile << " ";
                }
                outFile << ";";
            }
            outFile << "\n";
        }
        outFile.close();
    }

    void toXyzFormatted(vector<vector<vector<glm::vec3>>> contours, string filename) {
        std::ofstream outFile;
        outFile.open(filename);

        for (auto contourList: contours) {
            for (auto contour: contourList) {
                for (auto point: contour) {
                    outFile << std::to_string(point.x) + " ";
                    outFile << std::to_string(point.y) + " ";
                    outFile << std::to_string(point.z);
                    outFile << "\n";
                }
            }
        }

        outFile.close();
    }

    double getHighestPoint(Triangle tri) {
        if (tri.v1.y >= tri.v2.y && tri.v1.y >= tri.v3.y) {
            return tri.v1.y;
        } else if (tri.v2.y >= tri.v3.y) {
            return tri.v2.y;
        } else {
            return tri.v3.y;
        }
    }

    double getLowestPoint(Triangle tri) {
        if (tri.v1.y <= tri.v2.y && tri.v1.y <= tri.v3.y) {
            return tri.v1.y;
        } else if (tri.v2.y <= tri.v3.y) {
            return tri.v2.y;
        } else {
            return tri.v3.y;
        }
    }

    std::pair<double, double> getLowestHighestPoint(vector<Triangle> mesh) {
        double lowest = std::numeric_limits<double>::infinity();
        double highest = -std::numeric_limits<double>::infinity();

        for (auto triangle: mesh) {
            double triangleLowest = getLowestPoint(triangle);
            double triangleHighest = getHighestPoint(triangle);

            if (triangleLowest < lowest) {
                lowest = triangleLowest;
            }
            if (triangleHighest > highest) {
                highest = triangleHighest;
            }
        }

        return std::make_pair(lowest, highest);
    }
}  // namespace ContourExporter.