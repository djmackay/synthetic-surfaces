#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
#include <cmath>
#include <vector>
#include <glm/vec3.hpp>
#include <Intersection.hpp>
#include <PointSampler.hpp>
#include <PointSet.hpp>
#include <SamplerConfigurationError.hpp>

using namespace std;

// TODO(dmackay): this whole file needs a lot of refactoring.

ofstream fout;

struct vertex{
    double x;
    double y;
    double z;
};


struct SurfaceInformation{
    unsigned long verticesPerContour;
    unsigned long numContours;
};


struct SamplerConfig {
    double pointDistance;
    double contourDistance;
};


// Checks whether a point is contained within a surface.
bool containsPoint(const vertex point) {
    return false;  // TODO(dmackay):
}

string getplyHeader(const SurfaceInformation surfaceInfo) {
    string header;
    header += "ply\n";
    header += "format ascii 1.0\n";
    header += "element vertex " + to_string(surfaceInfo.verticesPerContour * surfaceInfo.numContours) + "\n";
    header += "property float x\n";
    header += "property float y\n";
    header += "property float z\n";
    header += "element face " + to_string(surfaceInfo.verticesPerContour * (surfaceInfo.numContours - 1) * 2) + "\n";
    header += "property list uchar int vertex_index\n";
    header += "end_header\n";

    return header;
}

//
vector<vector<int>> getContourList() {
    const int vertices_per_contour = 36;  // TODO(dmackay): should be a param.
    const int num_contours = 25;  // TODO(dmackay): should be a param.
    const double degrees_to_radians = 3.14159265/180.0;  // TODO(dmackay):
    const float xoff[] = {0, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.25, 0.3, 0.37, 0.45, 0.55, 0.7, 0.9, 1.1, 1.25, 1.5, 1.75,2, 2.25, 2.6, 3, 3.5, 4};  // TODO(dmackay)

    vector<vector<int>> contour_list;

    vertex currentVertex{0.0, 0.0, 0.0};
    int vertex_number = 0;

    for(int i = 0; i < num_contours; i++)
    {
        vector<int> vertex_list;

        currentVertex.y = i;
        for(int j = 0; j < vertices_per_contour; j++)
        {
            currentVertex.x = xoff[i] + 3.0 * cos(j*10*degrees_to_radians);
            currentVertex.z = -3.0 * sin(j*10*degrees_to_radians);
            cout << currentVertex.x << " " << currentVertex.y << " " << currentVertex.z << " " << endl;

            vertex_list.emplace_back(vertex_number);
            vertex_number += 1;
        }
        contour_list.emplace_back(vertex_list);
    }

    return contour_list;
}

// Outputs a generated surface in .ply file format.
void to_ply() {
    const float xoff[] = {0, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.25, 0.3, 0.37, 0.45, 0.55, 0.7, 0.9, 1.1, 1.25, 1.5, 1.75,2, 2.25, 2.6, 3, 3.5, 4};
	const double cdr = 3.14159265/180.0;
    vertex currentVertex{0.0, 0.0, 0.0};
	fout.open("Contours.ply", ios::out);

	const int vertices_per_contour = 36;
	const int num_contours = 25;

	vector<vector<int>> contour_list;

	fout << "ply" << endl;
	fout << "format ascii 1.0" << endl;
	fout << "element vertex " << vertices_per_contour * num_contours << endl;
	fout << "property float x" << endl;
	fout << "property float y" << endl;
	fout << "property float z" << endl;
	fout << "element face " << vertices_per_contour * (num_contours - 1) * 2<< endl;
	fout << "property list uchar int vertex_index" << endl;
    fout << "end_header" << endl;

	int vertex_number = 0;

	for(int i = 0; i < num_contours; i++)
	{
        vector<int> vertex_list;

        currentVertex.y = i;
		for(int j = 0; j < vertices_per_contour; j++)
		{
			currentVertex.x = xoff[i] + 3.0 * cos(j*10*cdr);
			currentVertex.z = -3.0 * sin(j*10*cdr);
			fout << currentVertex.x << " " << currentVertex.y << " " << currentVertex.z << " " << endl;

			vertex_list.emplace_back(vertex_number);
            vertex_number += 1;
		}
		contour_list.emplace_back(vertex_list);
	}

    int num_faces = 0;

	for (int j = 1; j < num_contours; j++) {
	    for (int i = 0; i < vertices_per_contour; i++) {
            if (i == 0){
                int last = vertices_per_contour - 1;
                int tl = contour_list.at(j-1).at(last);
                int tr = contour_list.at(j-1).at(i);
                int bl = contour_list.at(j).at(last);
                int br = contour_list.at(j).at(i);

                fout << "3 " << tl << " " << tr << " " << br << endl;
                fout << "3 " << tl << " " << br << " " << bl << endl;
            } else {
                int tl = contour_list.at(j - 1).at(i - 1);
                int tr = contour_list.at(j - 1).at(i);
                int bl = contour_list.at(j).at(i - 1);
                int br = contour_list.at(j).at(i);

                fout << "3 " << tl << " " << tr << " " << br << endl;
                fout << "3 " << tl << " " << br << " " << bl << endl;
            }
            num_faces += 1;
	    }
	}

	cout << num_faces << endl;

	fout.close();
}

void to_txt(){
    const float xoff[] = {0, 0.03, 0.06, 0.09, 0.12, 0.15, 0.18, 0.21, 0.25, 0.3, 0.37, 0.45, 0.55, 0.7, 0.9, 1.1, 1.25, 1.5, 1.75,2, 2.25, 2.6, 3, 3.5, 4};
    double cdr = 3.14159265/180.0;
	vertex currentVertex{0.0, 0.0, 0.0};
	fout.open("Contours.txt", ios::out);

	for(int i = 0; i < 25; i++)
	{
        currentVertex.y = i;
		for(int j = 0; j < 36; j++)
		{
            currentVertex.x = xoff[i] + 3.0 * cos(j*10*cdr);
            currentVertex.z = -3.0 * sin(j*10*cdr);
			fout << currentVertex.x << " " << currentVertex.y << " " << currentVertex.z << " " ;
		}
		fout << endl;
	}
	
	fout.close();
}

double getHighestPoint(Triangle tri) {
    if (tri.v1.y >= tri.v2.y && tri.v1.y >= tri.v3.y) {
        return tri.v1.y;
    } else if (tri.v2.y >= tri.v3.y) {
        return tri.v2.y;
    } else {
        return tri.v3.y;
    }
}

double getLowestPoint(Triangle tri) {
    if (tri.v1.y <= tri.v2.y && tri.v1.y <= tri.v3.y) {
        return tri.v1.y;
    } else if (tri.v2.y <= tri.v3.y) {
        return tri.v2.y;
    } else {
        return tri.v3.y;
    }
}

pair<double, double> getLowestHighestPoint(vector<Triangle> mesh) {
    double lowest = numeric_limits<double>::infinity();
    double highest = -numeric_limits<double>::infinity();

    for (auto triangle: mesh) {
        double triangleLowest = getLowestPoint(triangle);
        double triangleHighest = getHighestPoint(triangle);

        if (triangleLowest < lowest) {
            lowest = triangleLowest;
        }
        if (triangleHighest > highest) {
            highest = triangleHighest;
        }
    }

    return std::make_pair(lowest, highest);
}

vector<vector<glm::vec3>> findDistinctPointSets(vector<glm::vec3> points, double threshold) {
    // Begin at a random point, create a new contour.
    vector<vector<glm::vec3>> contours;
    vector<glm::vec3> currentContour;

    // Get the first point from the vector and remove it.
    auto currentPoint = points.front();
    points.erase(points.begin());
    currentContour.emplace_back(currentPoint);

    while (!points.empty()) {
        auto closest = points.begin();
        double distToClosest = std::numeric_limits<double>::infinity();
        // Iterate through all remaining points. Find the closest point and add it to the contour.
        for (auto it = points.begin(); it != points.end(); it++) {
            if (glm::distance(currentPoint, *it) < distToClosest) {
                closest = it;
                distToClosest = glm::distance(currentPoint, *closest);
            }
        }

        // If we found no points to add, we must create a new contour
        if (std::isinf(distToClosest) || distToClosest > threshold) {
            contours.emplace_back(currentContour);
            // Get the first point in the vector, and erase it.
            currentPoint = points.front();
            points.erase(points.begin());
            // Reset the contour vector, then add the first point to it.
            currentContour.clear();
            currentContour.emplace_back(currentPoint);
        } else {
            currentPoint = *closest;
            currentContour.emplace_back(*closest);
            points.erase(closest);
        }
    }

    contours.emplace_back(currentContour);

    return contours;
}

vector<vector<glm::vec3>> createContours(vector<Triangle> triangles, SamplerConfig samplerConfig) {
    if (samplerConfig.contourDistance <= 0.0) {
        throw SamplerConfigurationError();
    }

    double startHeight;
    double endHeight;
    tie(startHeight, endHeight) = getLowestHighestPoint(triangles);

    vector<vector<glm::vec3>> contours;

    double currentHeight = startHeight + 0.5 * samplerConfig.contourDistance;
    while (currentHeight < endHeight) {
        // Create a plane at current height.
        Plane plane{glm::vec3{0.0, currentHeight, 0.0}, glm::vec3{0.0, 1.0, 0.0}};
        vector<glm::vec3> points;

        // Generate edges, sample points and add to (currently global) point set.
        for (auto triangle: triangles) {
            if (Intersection::intersects(plane, triangle)) {
                auto edge = Intersection::computeIntersection(plane, triangle);
                auto newPoints = PointSampler::samplePoints(edge, samplerConfig.pointDistance);
                points.insert(points.end(), newPoints.begin(), newPoints.end());
            }
        }

        double threshold = samplerConfig.pointDistance * 2;
        auto distinctPointSets = findDistinctPointSets(points, threshold);

        for (auto& pointSet : distinctPointSets) {
            PointSet unordered;
            unordered.addPoints(pointSet);
            auto orderedPoints = unordered.toOrderedPointSet(unordered.computeCentroid());
            if (!orderedPoints.empty()) {
                contours.emplace_back(orderedPoints);
            }
        }

        currentHeight += samplerConfig.contourDistance;
    }
    return contours;
}

void toXYZ(vector<vector<glm::vec3>> contours, const std::string& filename) {
    ofstream outFile;
    outFile.open(filename);

    for (auto contour: contours) {
        for (auto point: contour) {
            outFile << to_string(point.x) + " ";
            outFile << to_string(point.y) + " ";
            outFile << to_string(point.z);
            outFile << "\n";
        }
    }
    outFile.close();
}

void toTxt(vector<vector<glm::vec3>> contours, const std::string& filename) {
    ofstream outFile;
    outFile.open(filename);

    for (auto contour: contours) {
        for (auto point: contour) {
            outFile << to_string(point.x) + " ";
            outFile << to_string(point.y) + " ";
            outFile << to_string(point.z);
            outFile << "\n";
        }
        outFile << "\n";
    }

    outFile.close();
}

void toSeperateContours(vector<vector<glm::vec3>> contours) {
    string filePrefix = "new";
    string filePostfix = ".xyz";
    int i = 0;

    for (auto contour: contours) {
        ofstream outFile;
        string filename = filePrefix + std::to_string(i) + filePostfix;
        outFile.open(filename);
        i++;

        for (auto point: contour) {
            outFile << to_string(point.x) + " ";
            outFile << to_string(point.y) + " ";
            outFile << to_string(point.z);
            outFile << "\n";
        }
        outFile << "\n";
        outFile.close();
    }
}