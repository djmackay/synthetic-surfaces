#include "Surface.hpp"

namespace Geometry{
    using std::string;
    using std::stringstream;
    using std::vector;

    void Surface::addPoint(glm::vec3 point) {
        points.emplace_back(point);
    }

    void Surface::addPoint(glm::vec3 point, glm::vec3 normal) {
        points.emplace_back(point);
        normals.emplace_back(normal);
    }

    void Surface::addTriangle(Geometry::TriangleIndices tri) {
        indices.emplace_back(tri);
    }

    void Surface::addQuad(Geometry::QuadIndices quad) {
        // Convert to two triangles instead of storing the quad.
        // If quads are properly implemented later this can be changed.
        TriangleIndices tri1{quad.x, quad.y, quad.z};
        TriangleIndices tri2{quad.x, quad.z, quad.w};
        indices.emplace_back(tri1);
        indices.emplace_back(tri2);
    }

    // Returns the y value of the lowest point in the surface.
    double Surface::lowestYValue() {
        double lowest = std::numeric_limits<double>::infinity();
        for (auto point: points) {
            if (point.y < lowest) {
                lowest = point.y;
            }
        }

        return lowest;
    }

    // Returns the y value of the highest point in the surface.
    double Surface::highestYValue() {
        double highest = -std::numeric_limits<double>::infinity();
        for (auto point: points) {
            if (point.y > highest) {
                highest = point.y;
            }
        }

        return highest;
    }

    // Returns a vector of triangles. Triangles are stored as points in the returned data structure.
    // This is purely for convenience but is a significant waste of performance.
    vector<Triangle> Surface::toTriangles() {
        vector<Triangle> triangles;
        for (auto triangleIndicies: indices) {
            Triangle triangle {points[triangleIndicies.x],
                               points[triangleIndicies.y],
                               points[triangleIndicies.z]};

            triangles.emplace_back(triangle);
        }

        return triangles;
    }

    string Surface::toString() {
        stringstream surfaceStr{std::ios_base::out | std::ios_base::in};
        surfaceStr << "Surface.\n";
        surfaceStr << "Vertices:\n";

        for (auto point: points) {
            surfaceStr << "\tx: " << point.x << ", y: "  << point.y << ", z: " << point.z << "\n";
        }

        surfaceStr << "Normals:\n";
        for (auto normal: normals) {
            surfaceStr << "\tx: " << normal.x << ", y: "  << normal.y << ", z: " << normal.z << "\n";
        }

        surfaceStr << "Triangles:\n";
        for (auto triangle: indices) {
            surfaceStr << "\tindex 1: " << triangle.x << ", index 2: "  << triangle.y << ", index 3: " << triangle.z << "\n";
        }

        return surfaceStr.str();
    }
}  // namespace Geometry.