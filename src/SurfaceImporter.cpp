#include "SurfaceImporter.hpp"

namespace SurfaceImporter {
    using std::vector;
    using std::string;

    Geometry::Surface loadSurfacePly(const string& filename) {
        Geometry::Surface surface;

        std::ifstream inputFile(filename, std::ios::binary);

        if (!inputFile.is_open()) {
            throw ImportError("Failed to open file: " + filename);
        } else {
            string line;
            int numVertices = 0;
            unsigned int vertexElems = 0;
            int numFaces = 0;
            int faceElems = 0;

            // Process until we find element vertex.
            while (!boost::starts_with(line, "element vertex") && std::getline(inputFile, line)) {
                if (boost::starts_with(line, "element vertex")) {
                    vector<string> substrings;
                    boost::split(substrings, line, boost::is_any_of("\t "));
                    if (substrings.size() == 3) {
                        numVertices = std::stoi(substrings[2], nullptr, 10);
                    } else {
                       throw ImportError("Unexpected size of line beginning with element vertex");
                    }
                }
            }

            // Process until we find element face.
            while (!boost::starts_with(line, "element face") && std::getline(inputFile, line)) {
                if (boost::starts_with(line, "property")) {
                    vertexElems += 1;
                } else if (boost::starts_with(line, "element face")) {
                    vector<string> substrings;
                    boost::split(substrings, line, boost::is_any_of("\t "));
                    if (substrings.size() == 3) {
                        numFaces = std::stoi(substrings[2], nullptr, 10);
                    } else {
                        throw ImportError("");
                    }
                } else {
                    throw ImportError("Unexpected value when parsing vertex properties.");
                }
            }

            while (line != "end_header" && std::getline(inputFile, line)) {
                if (boost::starts_with(line, "property")) {
                    faceElems += 1;
                }
            }

            for (int vertex = 0; vertex < numVertices; vertex++){
                std::getline(inputFile, line);
                vector<string> substrings;
                boost::split(substrings, line, boost::is_any_of("\t "));

                if (substrings.size() == vertexElems) {
                    if (vertexElems == 3) {
                        // just the vertex information.
                        glm::vec3 point{std::stof(substrings[0]), std::stof(substrings[1]), std::stof(substrings[2])};
                        surface.addPoint(point);
                    } else if (vertexElems == 6) {
                        glm::vec3 point{std::stof(substrings[0]), std::stof(substrings[1]), std::stof(substrings[2])};
                        glm::vec3 normal{std::stof(substrings[3]), std::stof(substrings[4]), std::stof(substrings[5])};
                        surface.addPoint(point, normal);
                    } else {
                        throw ImportError("Unsupported vertex size.");
                    }
                } else {
                    throw ImportError("Unexpected vertex size.");
                }
            }

            for (int face = 0; face < numFaces; face++){
                std::getline(inputFile, line);
                vector<string> substrings;
                boost::split(substrings, line, boost::is_any_of("\t "));

                if (!substrings.empty()) {
                    int faceSize = std::stoi(substrings.front());
                    if (faceSize == 3) {
                        Geometry::TriangleIndices triangle{std::stoi(substrings[1], nullptr, 10),
                                                           std::stoi(substrings[2], nullptr, 10),
                                                           std::stoi(substrings[3], nullptr, 10)};
                        surface.addTriangle(triangle);
                    } else if (faceSize == 4) {
                        Geometry::QuadIndices quad{std::stoi(substrings[1], nullptr, 10),
                                                   std::stoi(substrings[2], nullptr, 10),
                                                   std::stoi(substrings[3], nullptr, 10),
                                                   std::stoi(substrings[4], nullptr, 10)};

                        surface.addQuad(quad);
                    } else {
                        throw ImportError("Unexpected face size.");
                    }

                }
            }
        }

        return surface;
    }

    namespace {
//        void loadPlyHeader() {
//
//        }
    }  // namespace.
}  // namespace SurfaceImporter.