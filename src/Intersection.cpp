#include "Intersection.hpp"

namespace Intersection {
    bool intersects(Plane plane, Triangle tri) {
        auto dist1 = plane.distanceToPoint(tri.v1);
        auto dist2 = plane.distanceToPoint(tri.v2);
        auto dist3 = plane.distanceToPoint(tri.v3);

        // Check whether all points are on the same side.
        if (dist1 < 0 && dist2 < 0 && dist3 < 0){
            return false;
        } else if (dist1 > 0 && dist2 > 0 && dist3 > 0) {
            return false;
        } else {
            return true;
        }
    }

    bool intersects(Plane plane, Edge edge) {
        auto dist1 = plane.distanceToPoint(edge.start);
        auto dist2 = plane.distanceToPoint(edge.end);

        // Check whether points are on the same side of the plane.
        return !((dist1 < 0 && dist2 < 0) || (dist1 > 0 && dist2 > 0));
    }

    glm::vec3 computeIntersection(Plane plane, Edge edge) {
        if (!intersects(plane, edge)){
            throw IntersectionError();
        }

        auto direction = edge.direction();
        auto w = edge.start - plane.point;
        auto a = -glm::dot(plane.normal, w);
        auto b = glm::dot(plane.normal, direction);
        auto r = a / b;
        glm::vec3 intersectionPoint = edge.start + direction * r;

        return intersectionPoint;
    }

    Edge computeIntersection(Plane plane, Triangle triangle) {
        // We know that if a triangle intersects, two edges (only) must intersect with a plane.
        auto edge1 = Edge{triangle.v1, triangle.v2};
        auto edge2 = Edge{triangle.v2, triangle.v3};
        auto edge3 = Edge{triangle.v3, triangle.v1};

        glm::vec3 origin{0.0, 0.0, 0.0};
        glm::vec3 endPoint{0.0, 0.0, 0.0};
        int intersectCount = 0;
        if (intersects(plane, edge1)) {
            origin = computeIntersection(plane, edge1);
            intersectCount++;
        }

        if (intersects(plane, edge2)) {
            if (intersectCount == 0) {
                origin = computeIntersection(plane, edge2);
            } else if (intersectCount == 1) {
                endPoint = computeIntersection(plane, edge2);
            }
            intersectCount++;
        }

        if (intersects(plane, edge3)) {
            if (intersectCount == 0) {
                origin = computeIntersection(plane, edge3);
            } else if (intersectCount == 1) {
                endPoint = computeIntersection(plane, edge3);
            }
        }

        if (intersectCount != 2) {
            //throw IntersectionError();
        }

        return Edge{origin, endPoint};
    }
}  // namespace Intersection.