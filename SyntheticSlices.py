# -*- coding: utf-8 -*-
"""
Program to generate slice data (both images and contour data)
for a given branching structure defined in 'Branches.png'
Interslice distance = 10 rows (changed from 25 rows)
"""
import numpy as np
from math import cos, sin, radians
import cv2

img = cv2.imread('Branches.png', cv2.IMREAD_GRAYSCALE)  #Graylevel image
outImg = np.full((900, 900), 255, dtype=int)
print(img.shape)

rad = 50
flag = False
xprev = 0
yprev = 0
indx = 0
INTERSLICE_GAP = 10

for row in range(0, img.shape[0], INTERSLICE_GAP):   #Inter-slice gap = 10 rows  
    outImg = np.full((1024, 1024), 255, dtype=int)
    colprev = 0
    filename = 'c'+str(indx)+'.txt'
    outFile = open(filename, "w+")
    
    # For each pixel in the row.
    for col in range(img.shape[1]):
        
        # If pixel is coloured.
        if(img[row, col] < 5):
            # Do not generate a contour where previous contour is coloured.
            if col-colprev < 10: continue

            colprev = col
            
            # Reduce size of contours as we move through the image.
            if(row < 243): rad = 50
            elif(row < 476): rad = 40
            elif(row < 631): rad = 30
            else: rad = 20
            outFile.write("%d\n" % 36)

            # Generate a contour around the pixel.
            for angle in range(0, 360, 10):
                angRad = radians(angle)

                # Create noise for the contour samples
                noise = np.random.uniform(-1.0, 1.0)*rad*0.05   #5% of rad
                x = col + (rad+noise)*cos(angRad)
                y = 450 + (rad+noise)*sin(angRad)
                ix = int(x)
                iy = int(y)
                if(angle > 1):
                    cv2.line(outImg, (xprev, yprev), (ix, iy), 0,1)
                else:
                    x0 = ix
                    y0 = iy
                xprev = ix
                yprev = iy
                outFile.write("%d  %d\n" %(ix, iy))

            cv2.line(outImg, (xprev, yprev), (x0, y0), 0,1)

    outFile.close()
    filename = 'a'+str(indx)+'.png'
    cv2.imwrite(filename, outImg)
    indx = indx + 1

            
