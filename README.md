Proof-of-concept application for generating point data sets from meshes.

# Building.

Build the program by creating a build directory inside the root of the project and run"
```cmake ..```, then ```make```.

# Running.

Default build output location is in the /bin directory. Run the output program "synthetic_surfaces" from this directory.

# General
This program creates point cloud and contour data sets from input meshes. Data sets can then be used for surface reconstruction and compared with original meshes to analyse reconstruction issues.
*  Outputs both point cloud data (.xyz) for implicit surface reconstruction techniques and ordered contour data for techniques which reconstruct from contours. 
* Variable resolution can be defined to create lower or higher quality data sets. Both the number of slices and distance between points can be configured.