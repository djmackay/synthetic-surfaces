#ifndef SYNTHETIC_MODELS_CONTOUREXPORTER_HPP
#define SYNTHETIC_MODELS_CONTOUREXPORTER_HPP

#include <sstream>
#include <fstream>
#include <iostream>
#include <tuple>
#include <vector>
#include <glm/vec3.hpp>
#include "Intersection.hpp"
#include "PointSampler.hpp"
#include "PointSet.hpp"
#include "SamplerConfigurationError.hpp"

namespace ContourExporter {
    struct SamplerConfig {
        double pointDistance;
        double contourDistance;
    };
    std::vector<std::vector<glm::vec3>> findDistinctPointSets(std::vector<glm::vec3> points, double threshold);
    std::vector<std::vector<std::vector<glm::vec3>>> createContoursFormatted(std::vector<Triangle> triangles, SamplerConfig);
    void toWhitespaceFormattedFile(std::vector<std::vector<std::vector<glm::vec3>>> contours, std::string filename);
    void toXyzFormatted(std::vector<std::vector<std::vector<glm::vec3>>> contours, std::string filename);

    double getHighestPoint(Triangle tri);
    double getLowestPoint(Triangle tri);
    std::pair<double, double> getLowestHighestPoint(std::vector<Triangle> mesh);
}  // namespace ContourExporter.

#endif //SYNTHETIC_MODELS_CONTOUREXPORTER_HPP
