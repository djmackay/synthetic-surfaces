#ifndef SYNTHETIC_MODELS_SURFACEIMPORTER_HPP
#define SYNTHETIC_MODELS_SURFACEIMPORTER_HPP

#include <fstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "Intersection.hpp"
#include "Surface.hpp"

namespace SurfaceImporter {
    class ImportError: public std::logic_error {
    public:
        ImportError(): std::logic_error("Unable to import file.") {};
        explicit ImportError(const std::string& msg) : std::logic_error(msg) { };
    };


    Geometry::Surface loadSurfacePly(const std::string& filename);
}  // namespace SurfaceImporter.

#endif //SYNTHETIC_MODELS_SURFACEIMPORTER_HPP
