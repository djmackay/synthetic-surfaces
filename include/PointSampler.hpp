#ifndef SYNTHETIC_MODELS_POINTSAMPLER_HPP
#define SYNTHETIC_MODELS_POINTSAMPLER_HPP

#include <glm/vec3.hpp>
#include <glm/geometric.hpp>
#include <vector>
#include "Edge.hpp"

namespace PointSampler {
    struct SampledPoints {
        std::vector<glm::vec3> points;
        double remainder;
    };

    // Generate sample points on a line that are a certain distance apart.
    /**
     *
     * @param edge
     * @param distance
     * @return
     */
    std::vector<glm::vec3> samplePoints(Edge edge, double distance);
    /**
     *
     * @param edge
     * @param distance
     * @return
     */
    SampledPoints samplePointsRemainder(Edge edge, double distance, double remainder);
}

#endif //SYNTHETIC_MODELS_POINTSAMPLER_HPP
