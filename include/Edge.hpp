#ifndef SYNTHETIC_MODELS_EDGE_HPP
#define SYNTHETIC_MODELS_EDGE_HPP

#include <cmath>
#include <glm/vec3.hpp>

class Edge {
public:
    Edge(glm::vec3 v1, glm::vec3 v2) : start(v1), end(v2){}

    glm::vec3 direction();
    double length();

    const glm::vec3 start;
    const glm::vec3 end;
};

#endif //SYNTHETIC_MODELS_EDGE_HPP
