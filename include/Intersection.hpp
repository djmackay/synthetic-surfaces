#ifndef SYNTHETIC_MODELS_INTERSECTIONS_H
#define SYNTHETIC_MODELS_INTERSECTIONS_H

#include <iostream>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/geometric.hpp>
#include "Plane.hpp"
#include "IntersectionError.hpp"
#include "Edge.hpp"

struct Triangle {
    glm::vec3 v1;
    glm::vec3 v2;
    glm::vec3 v3;
};

namespace Intersection {
    // Computes the line defined by a plane/triangle intersection.
    Edge computeIntersection(Plane, Triangle);
    // Compute the intersection between a line and a plane.
    glm::vec3 computeIntersection(Plane, Edge);
    // Computes whether a plane and a triangle intersect.
    bool intersects(Plane, Triangle);
    bool intersects(Plane, Edge);
}

#endif //SYNTHETIC_MODELS_INTERSECTIONS_H
