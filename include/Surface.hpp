#ifndef SYNTHETIC_MODELS_SURFACE_HPP
#define SYNTHETIC_MODELS_SURFACE_HPP

#include <glm/vec3.hpp>
#include <vector>
#include <sstream>
#include <string>
#include "Intersection.hpp"

namespace Geometry {
    struct TriangleIndices {
        int x;
        int y;
        int z;
    };

    struct QuadIndices {
        int x;
        int y;
        int z;
        int w;
    };

    class Surface {
    public:
        void addPoint(glm::vec3 point);
        void addPoint(glm::vec3 point, glm::vec3 normal);
        void addTriangle(TriangleIndices tri);
        void addQuad(QuadIndices quad);

        double lowestYValue();
        double highestYValue();

        std::vector<Triangle> toTriangles();
        std::string toString();
    private:
        std::vector<glm::vec3> points;
        std::vector<glm::vec3> normals;
        std::vector<TriangleIndices> indices;
    };
}  // namespace Geometry.

#endif //SYNTHETIC_MODELS_SURFACE_HPP
