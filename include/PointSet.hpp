#ifndef SYNTHETIC_MODELS_POINTSET_HPP
#define SYNTHETIC_MODELS_POINTSET_HPP

#define GLM_ENABLE_EXPERIMENTAL

#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <glm/geometric.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/constants.hpp>


class PointSet {
public:
    // Computes a centroid from a point set.
    glm::vec3 computeCentroid();
    void addPoint(glm::vec3);
    void addPoints(std::vector<glm::vec3>);
    std::vector<glm::vec3> toOrderedPointSet(glm::vec3 centroid);

private:
    std::vector<glm::vec3> points;
};

double computeAngle(glm::vec3 centroid, glm::vec3 point);

#endif //SYNTHETIC_MODELS_POINTSET_HPP
