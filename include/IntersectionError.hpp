#ifndef SYNTHETIC_MODELS_INTERSECTIONERROR_HPP
#define SYNTHETIC_MODELS_INTERSECTIONERROR_HPP

#include <stdexcept>


class IntersectionError: public std::logic_error {
public:
    IntersectionError() : std::logic_error("Attempt to compute intersection of non-intersecting primitives.") { };
};


#endif //SYNTHETIC_MODELS_INTERSECTIONERROR_HPP
