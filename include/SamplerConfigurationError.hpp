#ifndef SYNTHETIC_MODELS_SAMPLERCONFIGURATIONERROR_HPP
#define SYNTHETIC_MODELS_SAMPLERCONFIGURATIONERROR_HPP

#include <stdexcept>


class SamplerConfigurationError: public std::invalid_argument {
public:
    SamplerConfigurationError() : std::invalid_argument("Sampler configuration not specified correctly.") { };
};
#endif //SYNTHETIC_MODELS_SAMPLERCONFIGURATIONERROR_HPP
