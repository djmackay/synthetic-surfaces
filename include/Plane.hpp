#ifndef SYNTHETIC_MODELS_PLANE_H
#define SYNTHETIC_MODELS_PLANE_H

#include <glm/vec3.hpp>
#include <glm/geometric.hpp>

class Plane {
public:
    Plane(glm::vec3 p, glm::vec3 n): point(p), normal(glm::normalize(n)){};

    // Returns a (signed) distance to a point.
    double distanceToPoint(glm::vec3 point);

    const glm::vec3 point;
    const glm::vec3 normal;
};


#endif //SYNTHETIC_MODELS_PLANE_H
